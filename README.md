# AliyunProtectUninstall

卸载阿里（腾讯）监控，节约内存

### 阿里云
一键卸载:

`echo '151.101.108.133 raw.githubusercontent.com' > /etc/hosts && wget https://raw.githubusercontent.com/HXHGTS/AliyunProtectUninstall/master/APU.sh -O APU.sh && bash APU.sh && rm -f APU.sh`

### 腾讯云
一键卸载:

`echo '151.101.108.133 raw.githubusercontent.com' > /etc/hosts && wget https://raw.githubusercontent.com/HXHGTS/AliyunProtectUninstall/master/TPU.sh -O TPU.sh && bash TPU.sh && rm -f TPU.sh`


